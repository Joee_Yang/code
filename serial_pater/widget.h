#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtSerialPort>
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>
#include <QDateTimeAxis>
#include <QValueAxis>
#include <QTimer>
#include "QDateTime"

QT_CHARTS_USE_NAMESPACE
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
     int number;
     void initDraw();                            //初始化画布
private slots:

    void on_Open_clicked();

    void on_Close_clicked();

    void on_readyRead();

    void on_pushButton_clicked();

    void DrawLine();                            //画线（这里可以考虑使用函数数组）

private:
    Ui::Widget *ui;

    void system_init();

   QSerialPort global_port;

   QTimer *timer;                           //计时器
   QChart *chart;                           //画布
   QSplineSeries *series;                     //线
   QDateTimeAxis *axisX;                    //轴
   QValueAxis *axisY;
};
#endif // WIDGET_H
