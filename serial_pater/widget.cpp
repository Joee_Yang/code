#include "widget.h"
#include "ui_widget.h"

#include <QtSerialPort>
#include <QDebug>
#include <QMessageBox>
#include <QPalette>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    system_init();
    timer = new QTimer(this);
    connect(timer , SIGNAL(timeout()) , this , SLOT(DrawLine()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::system_init()
{
    global_port.setParity(QSerialPort::NoParity);
    global_port.setDataBits(QSerialPort::Data8);
    global_port.setStopBits(QSerialPort::OneStop);

    connect(ui->Open, SIGNAL(Open_clicked()),this,SLOT(on_Open_clicked()));
    connect(ui->Close , SIGNAL(Close_clicked()) , this , SLOT(on_Close_clicked()));
    connect(&global_port ,SIGNAL(readyRead()) , this ,SLOT(on_readyRead()));

}
void Widget::initDraw()
{
    QPen peny(Qt::darkRed , 3 , Qt::SolidLine , Qt::RoundCap , Qt::RoundJoin);
    chart = new QChart();
    series = new QSplineSeries();
    axisX = new QDateTimeAxis();
    axisY = new QValueAxis();

    chart->legend()->hide();        //隐藏图例
    chart->addSeries(series);       //把线添加到chart中
    axisX->setTickCount(10);   //设置坐标轴格数
    axisY->setTickCount(5);

    axisX->setFormat("hh:mm:ss");       //设置线式格式
    axisY->setMin(0);       //设置最小值
    axisY->setMax(100);
    axisX->setTitleText("时间");
    axisY->setLinePenColor(QColor(Qt::darkBlue));       //设置坐标轴颜色
    axisY->setGridLineColor(QColor(Qt::darkBlue));
    //axisY->setGridLineColor(QColor(Qt::darkBlue));
    axisY->setGridLineVisible(false);        //设置Y轴网格不显示
    axisY->setLinePen(peny);
    axisX->setLinePen(peny);

    chart->addAxis(axisX , Qt::AlignBottom);
    chart->addAxis(axisY , Qt::AlignLeft);

    series->attachAxis(axisX);
    series->attachAxis(axisY);

    axisY->setTitleText("y1");

    ui->widget->setChart(chart);
    ui->widget->setRenderHint(QPainter::Antialiasing);

}
void Widget::DrawLine()
{


    QDateTime currentTime = QDateTime::currentDateTime();
    //设置坐标轴的线式范围
    chart->axisX()->setMin(QDateTime::currentDateTime().addSecs(-60*1));
    chart->axisX()->setMax(QDateTime::currentDateTime().addMSecs(0));

    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    //number = qrand()%99;

    //增加新的点到曲线末尾
    series->append(currentTime.toMSecsSinceEpoch() , number);


}
void Widget::on_Open_clicked()
{
    //ui->textEdit->setText("Open click success\r\n");
    switch(ui->Port->currentIndex())
    {
    case 1:
    global_port.setPortName("COM0");
        break;
    case 2:
    global_port.setPortName("COM1");
        break;
    case 3:
    global_port.setPortName("COM2");
        break;
    case 4:
    global_port.setPortName("COM3");
        break;
    case 5:
    global_port.setPortName("COM4");
        break;
    case 6:
    global_port.setPortName("COM5");
        break;
    case 7:
    global_port.setPortName("COM6");
        break;
    case 8:
    global_port.setPortName("COM7");
        break;
    case 9:
    global_port.setPortName("COM8");
        break;
    case 10:
    global_port.setPortName("COM9");
        break;
    case 11:
    global_port.setPortName("COM10");
        break;
    case 12:
    global_port.setPortName("COM11");
        break;
    case 13:
    global_port.setPortName("COM12");
        break;
    case 14:
    global_port.setPortName("COM13");
        break;
    case 15:
    global_port.setPortName("COM14");
        break;
    }

    switch(ui->Baud->currentIndex() )
    {
    case 8:
    global_port.setBaudRate(QSerialPort::Baud115200);
        break;
    case 7:
    global_port.setBaudRate(QSerialPort::Baud57600 );
        break;
    case 6:
    global_port.setBaudRate(QSerialPort::Baud38400);
        break;
    case 5:
    global_port.setBaudRate(QSerialPort::Baud19200);
        break;
    case 4:
    global_port.setBaudRate(QSerialPort::Baud9600);
        break;
    case 3:
    global_port.setBaudRate(QSerialPort::Baud4800);
        break;
    case 2:
    global_port.setBaudRate(QSerialPort::Baud2400);
        break;
    case 1:
    global_port.setBaudRate(QSerialPort::Baud1200);
        break;

    }
     global_port.open(QSerialPort::ReadWrite);
    //ui->textEdit->setText(tr("Port: %1 \n Baud: %2 " ).arg(global_port.portName()).arg(global_port.baudRate()));

}

void Widget::on_Close_clicked()
{
    global_port.close();
    // ui->textEdit->setText("Close click success\r\n");

}


void Widget::on_readyRead()
{
    QByteArray array = global_port.readAll();
    number = (array[0]-'0')*10 + (array[1]-'0');
    qDebug() << number;//qDebug()<<" ";
    /*if(ui->checkBox->checkState() == Qt::Checked)
    {
        ui->textEdit->insertPlainText(QString(array.toHex()).append(' '));
    }else
    {
        ui->textEdit->insertPlainText(QString(array).append(' '));
    }*/
}

void Widget::on_pushButton_clicked()
{
    initDraw();
    timer->start();
    timer->setInterval(1000);
}
